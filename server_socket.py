import socket
import threading
import pyodbc, datetime

HEADER = 64
PORT = 80
SERVER = 'localhost'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
maclist=['70:85:c2:fe:4c:55']
server.bind(ADDR)

def sqlserver(db):

    server = 'localhost'
    database = 'tareaSQL'
    username = 'sa'
    password = 'Admin.123'
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    date=datetime.date.today()
    fecha=str(date.day)+'/'+str(date.month)+'/'+str(date.year)
    db.append(fecha)


    insert_query = ''' INSERT INTO info (id_sensor,temp,humedad,SampleTime,FechaIngreso)
                        VALUES (?,?,?,?,?);'''
    
    
    cursor.execute(insert_query,db)
    cnxn.commit()
    
    

    
def decrypt(word):
    cont=0
    mensaje=''
    for letter in word:
        cont+=1
    while cont>0:
        cont-=2
        mensaje+=word[cont]
    return mensaje



db=[]
def handle_client(conn, addr):
    i=0
    j=0
    print(f"[NEW CONNECTION] {addr} connected.")

    print(f"Checking MAC Address...")
    
    connected = True
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)
            if msg == DISCONNECT_MESSAGE:
                connected = False

            if i==0:
                i+=1
                if msg not in maclist:
                    print("MAC Address no válida")
                    connected=False
                if msg in maclist:
                    print("MAC Address Reconocida")
            if i==1:
                conn.send("Msg received".encode(FORMAT))
                i+=1
            if i>=2:
                if msg not in maclist:
                    decrypt(msg)                    
                    print(f"[{addr}] {decrypt(msg)}")
                    conn.send("Msg received".encode(FORMAT))
                    if j<4 :
                        db.append(decrypt(msg))
                        j+=1
                    if j>3 and msg!=DISCONNECT_MESSAGE:
                        db[0]=int(db[0])
                        db[1]=float(db[1])
                        print(db)
                        sqlserver(db)
                if msg in maclist:
                    conn.send("MAC Validada".encode(FORMAT))


    conn.close()


def start():
    server.listen()
    print(f"[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")

print("[STARTING] server is running.....")
start()
