import socket, re, uuid, random
import random, datetime

HEADER = 64
PORT = 80
SERVER = 'localhost'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
k=0

mac=(':'.join(re.findall('..', '%012x' % uuid.getnode()))) 

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(ADDR)

def send(msg):

    message=msg.encode(FORMAT)

    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)

    send_length += b' '*(HEADER-len(send_length))

    client.send(send_length)
    client.send(message)
    print(client.recv(2048).decode(FORMAT))

def encrypt(letras):
    cont=0
    men=''
    for letter in letras:
        cont+=1
    while cont>0:
        cont-=1
        men+=letras[cont]+str(random.randint(0,9))
    return(men)
    
j=round(random.uniform(1,20))
id_sensor=str(j)
temperatura=str(round(random.uniform(1,100),2))
hum=round(random.uniform(1,100))
humedad=str(hum)+'%'
now=datetime.datetime.now()
sampletime=str(now.hour)+':'+str(now.minute)+':'+str(now.second)

if k<=1:
    send(mac)
    k=2
    input()
if k>1:
    send(encrypt(id_sensor))
    send(encrypt(temperatura))
    send(encrypt(humedad))
    send(encrypt(sampletime))
